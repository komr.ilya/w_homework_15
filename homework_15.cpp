#include <iostream>


void PrintEvenOdd(int num, int evenodd)
{
	while (evenodd <= num)
	{
		std::cout << '\n' << evenodd;
		evenodd += 2;
	}
}


int main()
{
	int number, EvenOdd;

	std::cout << "Enter the number: ";
	std::cin >> number;

	std::cout << "Enter 0 for Even or 1 for Odd: ";
	std::cin >> EvenOdd;

	PrintEvenOdd(number, EvenOdd);

	return 0;
}